﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ass1
{
    public class Program
    {
        static void Main()
        {
            Console.Write("Nhap so luong giao vien: ");
            int soluong = int.Parse(Console.ReadLine());

            List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

            for(int i = 0; i < soluong; i++)
            {
                Console.WriteLine($"Nhap thong tin giao vien {i + 1}:");
                Console.Write("Ho ten: ");
                string hoTen = Console.ReadLine();
                Console.Write("Nam sinh: ");
                int namSinh = int.Parse(Console.ReadLine());
                Console.Write("Luong co ban: ");
                double luongCoBan = double.Parse(Console.ReadLine());
                Console.Write("He so luong: ");
                double heSoLuong = double.Parse(Console.ReadLine());

                GiaoVien gv = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
                danhSachGiaoVien.Add(gv);
            }

            double luongThapNhat = danhSachGiaoVien[0].TinhLuong();
            GiaoVien giaoVienLuongThapNhat = danhSachGiaoVien[0];

            foreach (var gv in danhSachGiaoVien)
            {
                if (gv.TinhLuong() < luongThapNhat)
                {
                    luongThapNhat = gv.TinhLuong();
                    giaoVienLuongThapNhat = gv;
                }
            }

            Console.WriteLine("Thong tin giao vien co luong thap nhat: ");
            giaoVienLuongThapNhat.XuatThongTin();

            Console.Read();
        }
    }
}
