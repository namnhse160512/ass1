﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ass1
{
    class GiaoVien : NguoiLaoDong
    {
        public double HeSoLuong { get; set; }

        public GiaoVien() { }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
        : base(hoTen, namSinh, luongCoBan)
        {
            HeSoLuong = heSoLuong;
        }

        public void NhapThongTin(double heSoLuong)
        {
            HeSoLuong = heSoLuong;
        }

        public override double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }

        public void XuatThongTin()
        {
            Console.WriteLine($"Ho va Ten: {HoTen}, Nam sinh: {NamSinh}, Luong co ban: {LuongCoBan}, He so luong: {XuLy()}, Luong: {TinhLuong()}");
        }
        public double XuLy()
        {
            return HeSoLuong += 0.6;
        }
    }

    
}
