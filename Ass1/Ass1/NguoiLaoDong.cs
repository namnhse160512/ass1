﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ass1
{
    abstract public class NguoiLaoDong
    {
        public string HoTen { get; set; }
        public int NamSinh { get; set; }
        public double LuongCoBan { get; set; }

        public NguoiLaoDong()
        {

        }

        public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }

        public void NhapThongTin(string hoten, int namsinh, double luongCoBan)
        {
            HoTen = hoten;
            NamSinh = namsinh;
            LuongCoBan= luongCoBan;
        }
        public virtual double TinhLuong()
        {
            return LuongCoBan;
        }
        public void XuatThongTin()
        {
            Console.WriteLine($"Ho va Ten: {HoTen}, Nam sinh: {NamSinh}, Luong co ban: {LuongCoBan}");
        }
    }
    
    

}
